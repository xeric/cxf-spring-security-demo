package hello.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "token")
public class TokenData {

    private String token;

    public TokenData(String token) {
        setToken(token);
    }

    public TokenData(){

    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
