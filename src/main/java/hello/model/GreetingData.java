package hello.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "greetings")
public class GreetingData {
    private String message;

    public GreetingData(String message) {
        setMessage(message);
    }

    public GreetingData(){

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
