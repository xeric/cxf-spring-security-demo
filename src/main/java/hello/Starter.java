package hello;

import org.apache.cxf.transport.servlet.CXFServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.session.HashSessionIdManager;
import org.eclipse.jetty.server.session.HashSessionManager;
import org.eclipse.jetty.server.session.SessionHandler;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;

import javax.servlet.DispatcherType;
import java.util.EnumSet;

public class Starter {

    public static void main(String [] args) throws Exception {
        Server server = new Server(9999);
        server.setSessionIdManager(new HashSessionIdManager());
        HashSessionManager sessionManager = new HashSessionManager();
        SessionHandler sessions = new SessionHandler(sessionManager);

        final ServletHolder servletHolder = new ServletHolder(new CXFServlet());
        final ServletContextHandler context = new ServletContextHandler();
        context.setContextPath("/");
        context.addServlet(servletHolder, "/rest/*");
        context.addEventListener(new ContextLoaderListener());

        context.setInitParameter("contextClass", AnnotationConfigWebApplicationContext.class.getName());
        context.setInitParameter("contextConfigLocation", AppConfig.class.getName());

        context.addFilter(
            new FilterHolder(new DelegatingFilterProxy("springSecurityFilterChain")),
            "/*", EnumSet.allOf(DispatcherType.class));
        context.setHandler(sessions);
        server.setHandler(context);

        server.start();
        server.join();
    }
}
