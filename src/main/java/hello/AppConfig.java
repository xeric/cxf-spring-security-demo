package hello;

import hello.service.AnonymousService;
import hello.service.HelloService;
import hello.service.LoginService;
import hello.service.SecuredService;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.provider.JAXBElementProvider;
import org.apache.cxf.jaxrs.provider.json.JSONProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import javax.ws.rs.core.Application;
import javax.ws.rs.ext.RuntimeDelegate;
import java.util.Arrays;

@Configuration
@Import(SecurityConfig.class)
@EnableWebSecurity
public class AppConfig {

    @Bean(destroyMethod = "shutdown")
    public SpringBus cxf() {
        return new SpringBus();
    }

    @Bean
    @DependsOn("cxf")
    public org.apache.cxf.endpoint.Server jaxRsServer() {
        JAXRSServerFactoryBean factory = RuntimeDelegate.getInstance().createEndpoint(
            jaxRsApiApplication(), JAXRSServerFactoryBean.class);

        factory.setServiceBean(Arrays.asList(helloService(), loginService(),
            anonymousService(), securedService()));
        factory.setAddress(factory.getAddress());
        factory.setProviders(Arrays.asList(new JAXBElementProvider(), new JSONProvider<>()));
        return factory.create();
    }

    @Bean
    public HelloService helloService() {
        return new HelloService();
    }

    @Bean
    public LoginService loginService() {
        return new LoginService();
    }

    @Bean
    public AnonymousService anonymousService(){
        return new AnonymousService();
    }

    @Bean
    public SecuredService securedService(){
        return new SecuredService();
    }

    private Application jaxRsApiApplication() {
        return new DemoApplication();
    }


}
