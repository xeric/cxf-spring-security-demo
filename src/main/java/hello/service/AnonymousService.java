package hello.service;

import hello.model.GreetingData;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/anonymous")
public class AnonymousService {

    @GET
    @Produces({"application/json"})
    public GreetingData greetingData(){
        return new GreetingData("Hello anonymous!");
    }

}
