package hello.service;

import hello.model.GreetingData;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/hello")
public class HelloService {

    @Produces({"application/json"})
    @GET
    public GreetingData getHello() {
        return new GreetingData("Hello");

    }
}
