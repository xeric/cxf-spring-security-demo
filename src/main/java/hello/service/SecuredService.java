package hello.service;


import hello.model.GreetingData;
import org.springframework.security.access.annotation.Secured;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/secured")
@Secured({"ROLE_USER"})
public class SecuredService {

    @Produces({"application/json"})
    @GET
    public GreetingData securedGreetings(){
        return new GreetingData("Secured Service!");
    }

}
